import argparse
import io
import json
import random

from datetime import datetime, timedelta

RANGE_HUMIDITY = list(range(40, 80)) + [None]
RANGE_TEMPERATURE = list(range(14, 26)) + [None]


def generate_fake_data(days=1):
    faked_data = []
    measured_at = datetime.now()
    measure_points = days * 48
    while measure_points:
        temperature = random.choice(RANGE_TEMPERATURE)
        measured_at = measured_at - timedelta(minutes=30)
        if temperature is not None:
            faked_data.append(dict(temperature=float(temperature), measured_at=measured_at.isoformat()))
        measure_points -= 1
    return faked_data

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--file_path', help='write data to this file', type=str, required=False)
    args = parser.parse_args()
    data = generate_fake_data(days=30)
    if args.file_path:
        with io.open(args.file_path, 'w') as fp:
            json.dump(list(reversed(data)), fp)
    else:
        print(json.dumps(data))