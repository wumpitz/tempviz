import random


DHT11 = None

RANGE_HUMIDITY = list(range(40, 80)) + [None]
RANGE_TEMPERATURE = list(range(14, 26)) + [None]

def read(sensor, pin):
    humidity = random.choice(RANGE_HUMIDITY)
    temp = random.choice(RANGE_TEMPERATURE)
    return (float(humidity) if humidity else None, float(temp) if temp else None)