from datetime import datetime
import argparse
import io
import json
import time

import Adafruit_DHT

DHT_SENSOR = Adafruit_DHT.DHT11
DHT_PIN = 4


def get_temperature(tries=1):
    temperatures = []
    while tries:
        temperature = Adafruit_DHT.read(DHT_SENSOR, DHT_PIN)[1]
        if temperature is not None:
            temperatures.append(temperature)
        time.sleep(2)
        tries -= 1
    if not temperatures:
        return None
    print(temperatures)
    return sum(temperatures) / len(temperatures)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('file_path', help='write data to this file', type=str)
    args = parser.parse_args()
    history_file = args.file_path
    temperature = get_temperature(tries=5)
    if temperature is not None:
        print('Temp={}C'.format(temperature))
        measured_at = datetime.now().isoformat()
        with io.open(history_file, 'r') as fp:
            data = json.load(fp)
        data.append(dict(temperature=temperature, measured_at=measured_at))
        with io.open(history_file, 'w') as fp:
            json.dump(data, fp)
        print('Written to: {}'.format(history_file))
    else:
        print('Sensor failure. Check wiring.')
