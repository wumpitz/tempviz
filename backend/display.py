from luma.emulator import device
from luma.core.error import DevicePermissionError
from luma.core.interface.serial import i2c
from luma.oled.device import ssd1306


def get_device():
    try:
        display_device = ssd1306(i2c(port=1, address=0x3C))
    except DevicePermissionError:
        display_device = device.pygame(mode='1')
    return display_device