#!/usr/bin/env python
# -*- coding: utf-8 -*-
from datetime import datetime, timedelta
import argparse
import io
import json
import time
from typing import OrderedDict

from display import get_device

try:
    import Adafruit_DHT
except ImportError:
    import emulator as Adafruit_DHT

from gpiozero import Button
from gpiozero.exc import BadPinFactory
from luma.core.virtual import viewport
from luma.core.render import canvas
from PIL import ImageFont


DHT_SENSOR = Adafruit_DHT.DHT11
DHT_PIN = 4


class BaseMode(object):
    def __init__(self, data_file_path):
        self._viewport = None
        self.file_path = data_file_path

    def render(self, virtual):
        pass

    def refresh(self):
        pass

    def is_active(self):
        return False


class CurrentTemperatureMode(BaseMode):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.font = ImageFont.truetype('DejaVuSans.ttf', 20)

    def get_temperature(self, tries=1):
        temperature = None
        while temperature is None and tries:
            temperature = Adafruit_DHT.read(DHT_SENSOR, DHT_PIN)[1]
            if temperature is None:
                time.sleep(2)
                tries -= 1
        return temperature
    
    def show_loading_information(self, virtual):
        text = 'messen...'
        with canvas(virtual) as draw:
            w, h = draw.textsize(text, font=self.font)
            draw.text(((virtual.width - w)/2, (virtual.height - h)/2), text=text, fill="white", font=self.font)

    def render(self, virtual):
        self.show_loading_information(virtual)
        temperature = self.get_temperature(tries=4)
        if temperature:
            text = '{:0.1f}C'.format(temperature)
        else:
            text = 'Error'
        with canvas(virtual) as draw:
            w, h = draw.textsize(text, font=self.font)
            draw.text(((virtual.width - w)/2, (virtual.height - h)/2), text=text, fill="white", font=self.font)


def get_data(file_path):
    with io.open(file_path, 'r') as fp:
        data = json.load(fp)
    return data


class HoursHistoryMode(BaseMode):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.hours = [1, 2, 4, 6, 12, 24]
    
    def evaluate_data(self, data):
        evaluated = OrderedDict([
            (hour, []) for hour in self.hours
        ])
        data = reversed(data)
        now = datetime.now()
        for measured_data in data:
            added = False
            measured_at = datetime.fromisoformat(measured_data['measured_at'])
            for hour, temperatures in evaluated.items():
                if measured_at >= now - timedelta(hours=hour):
                    temperatures.append(measured_data['temperature'])
                    added = True
            if not added:
                break
        for hour, temperatures in evaluated.items():
            count = len(temperatures)
            evaluated[hour] = (sum(temperatures) / count) if count else None
        return evaluated

    def render(self, virtual):
        evaluated_data = self.evaluate_data(get_data(self.file_path))
        with canvas(virtual) as draw:
            height = 0
            for hours, temperature in evaluated_data.items():
                if temperature is None:
                    continue
                text = 'Letzte {:>2}h: {:0.1f}'.format(hours, temperature)
                draw.text((0, height), text=text, fill="white")
                height += 8


class NightsHistoryMode(BaseMode):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.hour_range = [20, 21, 22, 23, 0, 1, 2, 3, 4, 5, 6, 7, 8]
        self._position = 0

    def evaluate_data(self, data):
        evaluated = OrderedDict()
        data = reversed(data)
        for measured_data in data:
            measured_at = datetime.fromisoformat(measured_data['measured_at'])
            if measured_at.hour not in self.hour_range:
                continue
            day = measured_at.date()
            if day not in evaluated:
                evaluated[day] = []
            evaluated[day].append(measured_data['temperature'])
        for day, temperatures in evaluated.items():
            evaluated[day] = (min(temperatures), max(temperatures), sum(temperatures) / len(temperatures))
        return evaluated

    def render(self, virtual):
        self._position = 0
        evaluated_data = self.evaluate_data(get_data(self.file_path))
        height = max(16 + len(evaluated_data) * 8, virtual._device.height)
        self._virtual = viewport(virtual._device, width=virtual.width, height=height)
        with canvas(self._virtual) as draw:
            height = 16
            draw.text((10, 0), text='Nächte', fill='white')
            draw.text((42, 8), text='min  max  avg', fill='white')
            for day, temperatures in evaluated_data.items():
                text = '{:%d.%m}: {:0.1f} {:0.1f} {:0.1f}'.format(day, *temperatures)
                draw.text((0, height), text=text, fill='white')
                height += 8
    
    def is_active(self):
        return self._position > self._virtual.height

    def refresh(self):
        if self._virtual.height < self._virtual._device.height:
            return
        self._position += 8
        try:
            self._virtual.set_position((0, self._position))
        except AssertionError:
            pass


class App(object):
    MODE_CURRENT_TEMP = 'TEMP'
    MODE_HOURS_HISTORY = 'HHIST'
    MODE_NIGHTS_HISTORY = 'NHIST'
    MODE_CHART = 'CHART'

    MODE_MAPPING = OrderedDict([
        (MODE_CURRENT_TEMP, CurrentTemperatureMode),
        (MODE_HOURS_HISTORY, HoursHistoryMode),
        (MODE_NIGHTS_HISTORY, NightsHistoryMode),
    ])

    inactive_delay = 10  #: Seconds of inactivity until the display will be put in standby mode

    def __init__(self, data_file_path):
        self._modes = {k: v(data_file_path=data_file_path) for k, v in self.MODE_MAPPING.items()}
        self.current_mode = self.MODE_CURRENT_TEMP
        self.device = get_device()
        self.viewport = viewport(self.device, width=self.device.width, height=self.device.height)
        self.last_rendered_at = datetime.now()
        self.is_inactive = False
        self._abort = False
    
    def abort(self):
        self._abort = True

    def check_inactive(self):
        if self.active_mode.is_active():
            return False
        return datetime.now() - self.last_rendered_at >= timedelta(seconds=self.inactive_delay)
    
    def deactivate(self):
        self.device.hide()
        self.is_inactive = True

    def activate(self):
        self.device.show()
        self.is_inactive = False

    def next(self):
        modes = list(self.MODE_MAPPING.keys())
        try:
            self.current_mode = modes[modes.index(self.current_mode) + 1]
        except IndexError:
            self.current_mode = modes[0]

    @property
    def active_mode(self):
        return self._modes[self.current_mode]

    def render(self, mode=None):
        self.current_mode = mode or self.current_mode
        print('Render mode: {}'.format(self.current_mode))

        self.active_mode.render(self.viewport)
        self.last_rendered_at = datetime.now()
        if self.is_inactive:
            self.activate()
    
    def run(self, injection=None):
        self.render()
        while not self._abort:
            if injection:
                injection()
            time.sleep(1)
            self.active_mode.refresh()
            if self.check_inactive():
                self.deactivate()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('file_path', help='write data to this file', type=str)
    args = parser.parse_args()

    def button_pressed(app):
        app.next()
        app.render()
    def injection(app):
        for event in app.device._pygame.event.get():
            if event.type == app.device._pygame.KEYDOWN:
                if event.key == app.device._pygame.K_UP:
                    button_pressed(app)
                    break
                if event.key == app.device._pygame.K_ESCAPE:
                    app.abort()
                    break
    
    app = App(data_file_path=args.file_path)
    try:
        button = Button(21)
    except BadPinFactory:
        pass
    else:
        button.when_pressed = lambda: button_pressed(app)
    injection_function = None
    if hasattr(app.device, '_pygame'):
        injection_function = lambda: injection(app)
    app.run(injection=injection_function)
